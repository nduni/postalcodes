package com.krystianrozbiecki.postalcodes.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PostalCode {

	@NotNull(message="wymagane")
	@Size(min=2,max=2, message="wymagana liczba dwucyfrowa")
	private String firstDigits;
	@NotNull(message="wymagane")
	@Size(min=3,max=3, message="wymagana liczba trzycyfrowa")
	private String secondDigits;
	
	public String getFirstDigits() {
		return firstDigits;
	}
	public void setFirstDigits(String firstDigits) {
		this.firstDigits = firstDigits;
	}
	public String getSecondDigits() {
		return secondDigits;
	}
	public void setSecondDigits(String secondDigits) {
		this.secondDigits = secondDigits;
	}
	
	
}
