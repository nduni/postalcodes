package com.krystianrozbiecki.postalcodes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class FullAddress {

	@JsonProperty("kod")
	private String postalCode;
	
	@JsonProperty("miasto")
	private String city;
	
	@JsonProperty("ulica")
	private String street;
	
	@JsonProperty("gmina")
	private String commune;
	
	@JsonProperty("powiat")
	private String county;
	
	@JsonProperty("wojewodztwo")
	private String voivodeship;
	
	@JsonProperty("dzielnica")
	private String district;

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCommune() {
		return commune;
	}

	public void setCommune(String commune) {
		this.commune = commune;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getVoivodeship() {
		return voivodeship;
	}

	public void setVoivodeship(String voivodeship) {
		this.voivodeship = voivodeship;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	@Override
	public String toString() {
		return "Pełny adres:\nkod pocztowy= " + postalCode + "\nmiasto= " + city + "\nulica= " + street + "\ngmina= "
				+ commune + "\n"+"powiat= " + county + "\nwojewództwo= " + voivodeship + "\ndzielnica= " + district;
	}
	
	
}
