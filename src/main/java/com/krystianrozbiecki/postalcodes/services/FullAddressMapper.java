package com.krystianrozbiecki.postalcodes.services;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.validation.Valid;

import org.springframework.ui.Model;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.krystianrozbiecki.postalcodes.domain.FullAddress;
import com.krystianrozbiecki.postalcodes.domain.PostalCode;

public class FullAddressMapper {

	public FullAddressMapper(@Valid PostalCode thePostalCode, Model model) {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

	
			Object[] mappedObject;
			try {
				mappedObject = objectMapper.readValue(new URL("http://kodpocztowy.jsoftware.org/api/"+thePostalCode.getFirstDigits()+"-"+thePostalCode.getSecondDigits()), Object[].class);
			
			ObjectMapper obj = new ObjectMapper();
			String jsonInString = obj.writeValueAsString(mappedObject[0]);
			FullAddress fullAddress = new ObjectMapper().readValue(jsonInString,FullAddress.class);
			model.addAttribute("fulladdress", fullAddress);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
}
