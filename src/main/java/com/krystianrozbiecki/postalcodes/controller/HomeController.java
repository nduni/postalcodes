package com.krystianrozbiecki.postalcodes.controller;

import java.io.IOException;
import java.net.URL;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.krystianrozbiecki.postalcodes.domain.FullAddress;
import com.krystianrozbiecki.postalcodes.domain.PostalCode;
import com.krystianrozbiecki.postalcodes.services.FullAddressMapper;

@Controller
public class HomeController {

	@GetMapping({"/","home"})
	public String showHome(Model theModel) {
		
		theModel.addAttribute("postalCode", new PostalCode());
		return "home";
	}
	
	@RequestMapping("searchByPostalCodeForm")
	public String searchByPostalCodeForm(@Valid
			@ModelAttribute( "postalCode") PostalCode thePostalCode, BindingResult theBindingResult, Model model ) {
			if  (theBindingResult.hasErrors()) {
				return "home";
			} else {
				FullAddressMapper fullAddressMapper= new FullAddressMapper(thePostalCode, model);
				return "result";
			}
		
	}

	
}
