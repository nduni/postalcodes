<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<html>
<head>
	<title>Wynik wyszukiwania po kodzie pocztowym</title>
</head>
<body>
	Wynik wyszukiwania dla kodu: ${postalCode.firstDigits }- ${postalCode.secondDigits }
	<br>
	Miasto: ${fulladdress.city}
	<br>
	Ulica: ${fulladdress.street}
	<br>
	Gmina: ${fulladdress.commune}
	<br>
	Powiat: ${fulladdress.county}
	<br>
	Województwo: ${fulladdress.voivodeship}
	<br>
	Dzielnica: ${fulladdress.district}
</body>
</html>