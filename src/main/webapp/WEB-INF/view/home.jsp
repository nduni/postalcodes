<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
	<title>Wyszukiwarka kodów pocztowych</title>
	
	<style>
		.error{color:red}
	</style>
</head>
<body>
	<h2>Wyszukiwanie na mapie kodów pocztowych</h2>
	<hr>	
	<form:form action="searchByPostalCodeForm" modelAttribute="postalCode">
	Podaj kod pocztowy: <form:input type="number"  size="2" maxlength="2" path="firstDigits"/><form:errors path="firstDigits" cssClass="error"/>-<form:input type="number"  size="3" maxlength="3" path="secondDigits"/><form:errors path="secondDigits" cssClass="error"/>
	<input type="submit" value="Wyszukaj"/>
	</form:form>
</body>
</html>